const sitedeploy = require('gulp-bitbucket-site-deploy');
const project = require('./package.json');

sitedeploy({
	username: 'iveseenthedark',
	version: project.version,
	source: './build',
	context: 'blog'
});
