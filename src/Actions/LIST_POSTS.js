import axios from 'axios';

/* Helper method to make the data directly expose below */
const get = async url => {
	const { data } = await axios.get(url);
	return data;
};

export default async ({ host_url }) => {
	// Load all the post, and get the content of the directories
	const post_list = await get(`${host_url}/posts`);

	// Load all the post files in one-shot
	const post_directories = await Promise.all(
		post_list.values.map(file => {
			return get(`${host_url}/${file.path}`);
		})
	);

	// Once we've got all the post, load the metadata and paths.
	const posts = [];
	for (let directory of post_directories) {
		// Get path to blog post
		const post = directory.values.filter(file => file.path.endsWith('post.md')).shift();

		// Load blog post metadata
		const metadata_path = directory.values
			.filter(file => file.path.endsWith('metadata.json'))
			.map(file => file.path)
			.shift();

		const metadata = await get(`${host_url}/${metadata_path}`);
		posts.push({ post, metadata, directory });
	}

	return posts;
};
