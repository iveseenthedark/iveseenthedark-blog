import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled, { keyframes } from 'styled-components';

const pulse = keyframes`
  0% {
		transform: scale(1);
  }
	
	50% {
		transform: scale(1.2)
	}
	
  100% {
		transform: scale(1);
  }
`;

const fade = keyframes`
 0% {
	 opacity: 0;
	}
	
	50% {
		opacity: 1;
	}
	
  100% {
		opacity: 0;
  }
`;

const Spinner = styled(FontAwesomeIcon)`
	flex: 1 1 0;
	font-size: 10rem;
	margin-top: calc(50vh - 10rem);
	animation: ${pulse} 2s ease-in infinite, ${fade} 2s ease-in infinite;
`;

export default () => <Spinner icon="stroopwafel" />;
