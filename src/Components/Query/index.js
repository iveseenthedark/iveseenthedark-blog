import React from 'react';
import Loading from '../Loading';

export default class Query extends React.Component {
	state = { content: null };

	componentWillMount = () => {
		const { action, variables } = this.props;
		action(variables).then(content => this.setState({ content }));
		// TODO Error handling here
	};

	render = () => {
		const { content } = this.state;
		const { children } = this.props;
		return content ? children(content) : <Loading />;
	};
}
