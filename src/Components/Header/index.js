import React from 'react';
import remcalc from 'remcalc';
import styled from 'styled-components';

const Header = styled.header`
	padding: ${remcalc(20)};
	height: ${remcalc(40)};
	background-color: #202125;
	color: white;
`;

const Title = styled.h1`
	margin: 0;
	white-space: nowrap;
	line-height: ${remcalc(40)};
	font-size: 4vw;
	@media (min-width: ${remcalc(1170)}) {
		font-size: ${remcalc(1170 * 0.04)};
	}
`;

export default () => (
	<Header>
		<Title>I V E S E E N T H E D A R K :: B L O G</Title>
	</Header>
);
