import React from 'react';
import { Link } from 'react-router-dom';

import remcalc from 'remcalc';
import styled from 'styled-components';

import BackgroundImage from 'react-background-image-loader';

import { withBitbucket } from '../../Utils/bitbucket-context';

const StyledBackgroundImage = styled(BackgroundImage)`
	position: relative;
	display: table;
	flex: 1 50%;
	height: ${remcalc(500)};

	background-position: 50%;
	background-size: cover;
	cursor: pointer;

	&:hover {
		filter: hue-rotate(180deg);
	}
	@media (max-width: ${remcalc(750)}) {
		flex: 1 100%;
	}
	@media (max-width: ${remcalc(970)}) {
		height: ${remcalc(420)};
	}
	@media (max-height: ${remcalc(570)}) {
		height: ${remcalc(420)};
	}
`;

const PostItemLink = styled(Link)`
	position: absolute;
	top: 0;
	left: 0;
	display: table;
	width: 100%;
	height: 100%;
`;

const PostItemOverlay = styled.span`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	margin: auto;
	width: ${remcalc(270)};
	height: ${remcalc(270)};

	mix-blend-mode: exclusion;
`;

const PostItemTitle = styled.h2`
	display: table-cell;
	vertical-align: middle;
	font-size: ${remcalc(25)};

	span {
		position: relative;
		z-index: 100;
		display: block;
		margin: auto;
		width: ${remcalc(135)};
		color: #202125;
	}
`;

const BlogListItem = ({ metadata, post, host_url }) => {
	// This is a bit of a hack. Need the directory path to load the image from
	const directory = post.path
		.split('/')
		.slice(0, -1)
		.join('/');

	return (
		<StyledBackgroundImage src={`${host_url}/${directory}/${metadata.image}`} placeholder={metadata.base64}>
			<PostItemLink to={`/${post.path}`}>
				<PostItemOverlay style={{ backgroundColor: metadata.palette[0] }} />
				<PostItemTitle>
					<span>{metadata.title}</span>
				</PostItemTitle>
			</PostItemLink>
		</StyledBackgroundImage>
	);
};

export default withBitbucket(BlogListItem);
