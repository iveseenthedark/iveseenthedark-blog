import React from 'react';
import styled from 'styled-components';
import ReactMarkdown from 'react-markdown';

import GET_POST from '../../Actions/GET_POST';
import Query from '../../Components/Query';

import { withBitbucket } from '../../Utils/bitbucket-context';

const PostContent = styled.div`
	margin: 0 auto;
	width: 80%;
	text-align: justify;
`;

const BackButton = styled.button`
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 2rem;
	line-height: 2rem;
	cursor: pointer;
`;

const Post = ({ host_url, match: { url }, history }) => {
	window.scrollTo(0, 0);
	return (
		<Query action={GET_POST} variables={{ host_url, url }}>
			{content => (
				<PostContent>
					<BackButton onClick={history.goBack}>{'<<<'}</BackButton>
					<ReactMarkdown source={content} escapeHtml={false} />
				</PostContent>
			)}
		</Query>
	);
};
export default withBitbucket(Post);
