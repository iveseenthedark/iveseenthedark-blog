import React from 'react';

import Query from '../../Components/Query';
import LIST_POSTS from '../../Actions/LIST_POSTS';

import PostListItem from '../../Components/PostListItem';
import { withBitbucket } from '../../Utils/bitbucket-context';

const PostList = ({ host_url }) => {
	return (
		<Query action={LIST_POSTS} variables={{ host_url }}>
			{posts => posts.map((post, i) => <PostListItem key={i} {...post} />)}
		</Query>
	);
};

export default withBitbucket(PostList);
