import React from 'react';

export const Host = 'https://api.bitbucket.org/2.0/repositories/iveseenthedark/iveseenthedark-posts/src/develop';
export const BitbucketContext = React.createContext(Host);
export const withBitbucket = Wrapped => {
	return props => (
		<BitbucketContext.Consumer>{host_url => <Wrapped {...props} host_url={host_url} />}</BitbucketContext.Consumer>
	);
};
