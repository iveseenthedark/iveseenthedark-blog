import { library } from '@fortawesome/fontawesome-svg-core';
import { faSkull, faStroopwafel } from '@fortawesome/free-solid-svg-icons';

library.add(faSkull);
library.add(faStroopwafel);
