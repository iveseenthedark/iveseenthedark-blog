import { injectGlobal } from 'styled-components';
import 'typeface-roboto-mono';

export default injectGlobal`
body {
	margin: 0;
	padding: 0;
}
`;
