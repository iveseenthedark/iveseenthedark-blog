import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import { BitbucketContext, Host } from './Utils/bitbucket-context';

import styled from 'styled-components';
import './Utils/global-styles';
import './Utils/font-library';

import Header from './Components/Header';
import PostListPage from './Pages/PostsList';
import Post from './Pages/Post';

const StyledApp = styled.div`
	color: #202125;
	text-align: center;
	font-family: 'Roboto Mono', sans-serif;
`;

const Main = styled.main`
	display: flex;
	flex-flow: row wrap;
	min-height: 90vh;
	font-size: large;
`;

class App extends Component {
	render() {
		return (
			<Router>
				<BitbucketContext.Provider value={Host}>
					<StyledApp>
						<Header />
						<Main>
							<Switch>
								<Route path="/" exact component={PostListPage} />
								<Route path="/*" component={Post} />
							</Switch>
						</Main>
					</StyledApp>
				</BitbucketContext.Provider>
			</Router>
		);
	}
}

export default App;
